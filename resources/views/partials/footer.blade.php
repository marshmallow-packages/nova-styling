<p class="mt-8 text-center text-xs text-80">
    &copy; {{ date('Y') }} <a href="https://marshmallow.dev" class="text-primary dim no-underline">Marshmallow</a> Theme v1.2.6
    <span class="px-1">&middot;</span>
    Laravel Nova v{{ \Laravel\Nova\Nova::version() }}
</p>
